<?php


$form_data = $_POST;
require_once( 'functions.php' );
$allowed_actions = array( 'generate_matrix_form', 'calculate');

if ( empty( $form_data['action'] ) || ! in_array( $form_data['action'], $allowed_actions ) ) {
	die( '0' );
}
//if the request = to generate_matrix_form then just pass it to function.php to return the tables
if ( 'generate_matrix_form' == $form_data['action'] ) {
	$P1_strategies_no = (int) $form_data['P1_strategies'];
	$P2_strategies_no = (int) $form_data['P2_strategies'];
	$P3_strategies_no = (int) $form_data['P3_strategies'];
	$P1_name = (string) $form_data['P1_name'];
	$P2_name =(string) $form_data['P2_name'];
	$P3_name =(string) $form_data['P3_name'];

	$response['HTML'] = generate_matrix_form( $P1_strategies_no, $P2_strategies_no ,$P3_strategies_no ,$P1_name , $P2_name , $P3_name );
} else if ( 'calculate' == $form_data['action'] ) {
	//if the request = to calculate then take the values as a one dimintional array then start to split it to multidimentional array
	$payoff = explode( ',', $form_data['payoffs'] );
	$count = count( $payoff );
	//file_put_contents('intermidiate.txt', var_export($payoff, TRUE)); // uncommnent this line to see the array that arrived form the tables
	session_start();
	$P1_strategies = $_SESSION['P1_strategies'];
	$P2_strategies = $_SESSION['P2_strategies'];
	$P3_strategies = $_SESSION['P3_strategies'];
	$build = $final = $third_dim = array();
	$counter =  0 ;
	for ($i=0; $i < $P3_strategies ; $i++) {
		for ($j=0; $j <$P1_strategies ; $j++) {
			for ($k=0; $k <$P2_strategies ; $k++) {
				array_push( $build, array( $payoff[ $counter ], $payoff[ $counter + 1 ] , $payoff[ $counter + 2 ] ) );
				$counter+=3 ;
				// split evey three element as a cell (array)
			}
			array_push( $final, $build );
			$build = array();
			// build the second player array
		}
		array_push( $third_dim, $final );
		$final = array();
		//build the the 3p array
	}
	file_put_contents('array.txt', var_export($third_dim, TRUE)); // pass it to functions.php to start the calculation for nash
	$response['HTML'] = calculate( $third_dim );
}else { $response = 0; }

echo json_encode( $response );

?>
