<?php require_once('functions.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Game Theory</title>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta charset="utf-8">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/styles.css" />



	<script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
	<script src="js/script.js?<?php echo time(); ?>"></script>
</head>

<body>

	<div class="">
		<h1 class="logo">I wanna' Play a Game</h1>
		<h3>Set the number of strategies for each player:</h3>
		<form id="myform">
			<div class="row">
				<div class="col-6">
					<label class="form-label" for="P1_name">First Player Name</label class="form-label"><br>
					<input class="form-control" name="P1_name" type="text" id="P1_name" placeholder="1st Player name" required><br>
				</div>
				<div class="col-6">
					<label class="form-label" for="P1_strategies_no"># of First Player Strategies</label class="form-label"><br>
					<input class="form-control" name="P1_strategies_no" type="number" id="P1_strategies_no" placeholder="#" min="1" max="5" required>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<label class="form-label" for="P2_name">second Player Name</label class="form-label"><br>
					<input class="form-control" name="P2_name" type="text" id="P2_name" placeholder="2nd Player name" required><br>
				</div>
				<div class="col-6">
					<label class="form-label" for="P2_strategies_no"># of Secound Player Strategies</label class="form-label"><br>
					<input class="form-control" name="P2_strategies_no" type="number" id="P2_strategies_no" placeholder="#" min="1" max="5" required>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<label class="form-label" for="P3_name">third Player Name</label class="form-label"><br>
					<input class="form-control" name="P3_name" type="text" id="P3_name" placeholder="3rd Player name" required><br>
				</div>
				<div class="col-6">
					<label class="form-label" for="P3_strategies_no"># of third Player Strategies</label class="form-label"><br>
					<input class="form-control" name="P3_strategies_no" type="number" id="P3_strategies_no" placeholder="#" min="1" max="5" required>
				</div>
			</div>
			<button name="set-strategies" type="submit" id="set-strategies" class="button button1">Start the Game</button>
		</form>
	</div>
	<script>
		$('#myform').validate({
			//when you submit the form
			submitHandler: function(e) {
				var myData = new FormData();
				myData.append("action", "generate_matrix_form");
				myData.append("P1_strategies", $("#P1_strategies_no").val());
				myData.append("P2_strategies", $("#P2_strategies_no").val());
				myData.append("P3_strategies", $("#P3_strategies_no").val());
				myData.append("P1_name", $("#P1_name").val());
				myData.append("P2_name", $("#P2_name").val());
				myData.append("P3_name", $("#P3_name").val());
				$.post({
					url: "ajax.php",
					data: myData,
					dataType: "json",
					processData: false,
					contentType: false,
					success: function(response, textStatus, jqXHR) {
						$("body").empty().html(response.HTML);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						//console.log( errorThrown );
					},
				});
				return false;
			}
		})


		//when random button is clicked generate random values in the cell in a specific way
		$(document).on("click", "#randomize", function(e) {
			$("style").remove();
			$(".result").remove();
			//console.log("random is clicked");
			e.preventDefault();
			$('input[name^="payoff"]').each(function() {
				var P1 = Math.floor(Math.random() * 21 - 10),
					P2 = Math.floor(Math.random() * 21 - 10),
					P3 = Math.floor(Math.random() * 21 - 10);
				$(this).val("(" + P1 + ", " + P2 + ", " + P3 + ")");
			});
			if ("disabled" == $("#calculate").attr("disabled")) {
				//$( '#calculate' ).removeAttr( 'disabled' );
			}
			return false;
		});

		//when you click calculate button just remove the brackets then pass it to ajax to format the matrix
		$(document).on("click", "#calculate", function(e) {
			e.preventDefault();
			//$( this ).attr( 'disabled','disabled' );
			var form = new FormData(),
				payoffs = new Array(),
				payoff = new Array();
			$('input[name^="payoff"]').each(function() {
				payoff = $(this)
					.val()
					.trim()
					.replace("(", "")
					.replace(")", "")
					.replace(" ", "")
					.split(",");
				payoffs.push(payoff);
			});
			form.append("action", "calculate");
			form.append("payoffs", payoffs);
			$.post({
				url: "ajax.php",
				data: form,
				dataType: "json",
				processData: false,
				contentType: false,
				success: function(response, textStatus, jqXHR) {
					$("#results").prepend(response.HTML);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(errorThrown);
				},
			});
			return false;
		});
	</script>
</body>

</html>