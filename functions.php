<?php

function generate_matrix_form($P1_strategies_no, $P2_strategies_no, $P3_strategies_no, $P1_name, $P2_name, $P3_name)
{
	$table_s = '<div class="row">';
	session_start();
	$_SESSION['failed_times'] = 0; //to output the number of times that the game played
	$_SESSION['P1_strategies'] = $P1_strategies_no; // Number of strategies for player 1
	$_SESSION['P2_strategies'] = $P2_strategies_no; // Number of strategies for player 2
	$_SESSION['P3_strategies'] = $P3_strategies_no; // Number of strategies for player 3
	// Generating the matrix table visualy (initialy empty)
	for ($i = 0; $i < $P3_strategies_no; $i++) {
		$header = '<tr class="table-dark"><th>' . $P1_name . '/' . $P2_name . '/' . $P3_name . ' S ' . chr($i + 49) . '</th>';
		$lines = '';
		for ($row = 0; $row < $P1_strategies_no; $row++) {
			for ($col = 0; $col <= $P2_strategies_no; $col++) {
				if (0 == $row) {
					if ($col == $P2_strategies_no) {
						$header .= '</tr>';
					} else {
						$header .= '<th>Strategy ' . chr($col + 97) . '</th>';
					}
				}
				if (0 == $col) {
					$lines .= '<tr><td>Strategy ' . chr($row + 65) . '</td>';
				} else {
					$lines .= '<td><input class="form-control" type="text" name="payoff[]" title="(P1, P2 , P3)" placeholder="(P1, P2 , P3)" /></td>';
					if ($col == $P2_strategies_no) {
						$lines .= '</tr>';
					}
				}
			}
		}
		$t_i = $i + 1;
		$table_s .= '<div class="col-4"><table class="table table-hover table-light table-sm no-' . $t_i . '">' . $header . $lines . '</table></div>	';
	}
	// $table_s = '' ;
	// for ($i=0; $i < $P3_strategies_no ; $i++) {
	// 	$table_s .= '<table class="table-fill left-table">' . $header . $lines . '</table>';
	// }
	$table_s .= '<div class="col-12 d-flex justify-content-around align-items-center">
	<p><button id="calculate" class="button button1" type="submit">Nash it!</button>
	<button id="randomize" class="button button1" type="submit"> Too lazy for that </button>
	<a href="index.php" class="button button1">Go back to game</a>
	<div id="results" class="results"></div>
	</div>';
	$table_s .= '</div>';

	return $table_s;
}
//function to calculate nash equilibrium
function calculate($payoffs)
{
	$P1_strategies = $_SESSION['P1_strategies']; // number of rows
	$P2_strategies = $_SESSION['P2_strategies']; // number of cols
	$P3_strategies = $_SESSION['P3_strategies']; // number of dimentions
	$pos = array();
	//so here how it gose , first of all genetate a look a like matrix for the payoffs that is empty
	//then traverse the rows and columns and dimentions(third player) and for every max value you find , just set that position +1
	// then when we find a position that =3 we know that the cell has been set by max(row) max(column) and max dim
	// and there is a nash equilibrium there .
	// Initialize the matrix values
	for ($dim = 0; $dim < $P3_strategies; $dim++) {
		for ($row = 0; $row < $P1_strategies; $row++) {
			for ($col = 0; $col < $P2_strategies; $col++) {
				// code...
				$pos[$dim][$row][$col] = 0;
			}
		}
	}

	$pos_row = $pos_col = $pos_dim = array();
	// [0] Get max of the columns means go throw the rows first
	for ($dim = 0; $dim < $P3_strategies; $dim++) {
		$max_col = array(-11, array(0, 0, 0)); // [ value_to_compare-1, [ array of coords ] ]
		for ($col = 0; $col < $P2_strategies; $col++) {
			for ($row = 0; $row < $P1_strategies; $row++) {
				if ($payoffs[$dim][$row][$col][0] > $max_col[0]) {
					$max_col[0] = $payoffs[$dim][$row][$col][0];
					$max_col[1] = array($dim, $row, $col);
				}
			}
			array_push($pos_col, $max_col[1] /* , $max_col[0]*/);
			$max_col = array(-11, array(0, 0, 0)); // [ value_to_compare-1, [ array of coords ] ]
		}
	}
	file_put_contents('pos_col.txt', var_export($pos_col, TRUE));

	// [1] Get max of the rows means go throw the columns first
	for ($dim = 0; $dim < $P3_strategies; $dim++) {
		$max_row = array(-11, array(-1, -1, -1)); // [ value_to_compare-1, [ array of coords ] ]
		for ($row = 0; $row < $P1_strategies; $row++) {
			for ($col = 0; $col < $P2_strategies; $col++) {
				if ($payoffs[$dim][$row][$col][1] > $max_row[0]) {
					$max_row[0] = $payoffs[$dim][$row][$col][1];
					$max_row[1] = array($dim, $row, $col);
				}
			}
			array_push($pos_row, $max_row[1]  /*, $max_row[0]*/);
			$max_row = array(-11, array(-1, -1, -1)); // [ value_to_compare-1, [ array of coords ] ]
		}
	}
	file_put_contents('pos_row.txt', var_export($pos_row, TRUE));

	//[2] get the maximum in the table for player 3 , going throw the dimentions
	for ($dim = 0; $dim < $P3_strategies; $dim++) {
		$max_dim = array(-11, array(-2, -2, -2)); // [ value_to_compare-1, [ array of coords ] ]
		for ($row = 0; $row < $P1_strategies; $row++) {
			for ($col = 0; $col < $P2_strategies; $col++) {
				if ($payoffs[$dim][$row][$col][2] > $max_dim[0]) {
					$max_dim[0] = $payoffs[$dim][$row][$col][2];
					$max_dim[1] = array($dim, $row, $col);
				}
			}
		}
		array_push($pos_dim, $max_dim[1]  /*, $max_dim[0]*/);
	}
	file_put_contents('pos_dim.txt', var_export($pos_dim, TRUE));

	//here we increment the position by one on columns then we do it the same for the rest
	foreach ($pos_col as $p) {
		$pos[$p[0]][$p[1]][$p[2]]++;
	}
	foreach ($pos_row as $p) {
		$pos[$p[0]][$p[1]][$p[2]]++;
	}
	// foreach ( $pos_dim as $p ) {
	// 	$pos[ $p[0] ][ $p[1] ][$p[2]]++;
	// }

	$str = '';
	$coloring_arr = array();
	foreach ($pos_dim as $p) {
		$var = ++$pos[$p[0]][$p[1]][$p[2]];
		if (3 == $var) {
			for ($dim = 0; $dim < $P3_strategies; $dim++) {
				for ($row = 0; $row < $P1_strategies; $row++) {
					for ($col = 0; $col < $P2_strategies; $col++) {
						if ($payoffs[$p[0]][$p[1]][$p[2]] == $payoffs[$dim][$row][$col]) {
							$index1 = $row + 1;
							$index2 = $col + 1;
							$index3 = $dim + 1;
							$str .= '[' . $index3 . '][' . $index1 . '][' . $index2 . '], '; // save the string to output it to the user
							array_push($coloring_arr,  array($index3, $index1, $index2) /*, $max_dim[0]*/);
						}
					}
				}
			}
		}
	}
	$failed_times = $_SESSION['failed_times']++;
	if (!empty($str)) {
		$str .= "<style>";
		foreach ($coloring_arr as $key) {
			$r_c = $key[1] + 1;
			$c_c = $key[2] + 1;
			$color = substr(str_shuffle('ABCDEF0123456789'), 0, 6);
			$str .= 'table.no-' . $key[0] . ' tr:nth-child(' . $r_c . ') td:nth-child(' . $c_c . ') {background-color:#' . $color . ';}';
		}
		$str .= "</style>";
		return '<div class="result"><strong>(' . $failed_times . ') : Nash equilibrium found</strong> at positions ' . $str . ' <a href="#" style ="text-decoration : none ; background-color:#' . $color . ';width : auto ; color :white ;  ">&nbsp(^_^) </a> <br/></div>';
	} else {
		return '<div class="result"><strong> (' . $failed_times . '): Opps !!!! no nash equilibrium found</strong> found.</div>';
	}
}
